$(document).ready(function(){
  $('#Carousel').carousel({
    interval: 10000
  })
  $('#myCarousel').on('slid.bs.carousel', function() {});
  
  $(".nav").on("click","a", function (even) {
    event.preventDefault();
    var hashtag = $(this).attr('href'),
        top = $(hashtag).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });
});
